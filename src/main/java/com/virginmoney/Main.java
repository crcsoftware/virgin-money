package com.virginmoney;

import com.virginmoney.service.*;


/**
 * Contains the main method to run the application
 */
public class Main {

    public static void main(String[] args) {

        System.out.println("Starting application");

        ControlService application = new ControlService();
        application.run();

        System.out.println("Terminated application");
    }

}
