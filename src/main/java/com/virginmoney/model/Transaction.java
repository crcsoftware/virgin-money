package com.virginmoney.model;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Transaction {

    private LocalDate transactionDate;
    private String vendor = "";
    private String type = "";
    private BigDecimal amount;
    private String category = "";

    public Transaction(LocalDate transactionDate, String vendor, String type, BigDecimal amount, String category) {
        this.transactionDate = transactionDate;
        this.vendor = vendor;
        this.type = type;
        this.amount = amount;
        this.category = category;
    }

    public LocalDate getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(LocalDate transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
