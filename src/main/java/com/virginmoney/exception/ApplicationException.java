package com.virginmoney.exception;

public class ApplicationException extends Exception {

    public ApplicationException(String errorMessage) {
        super(errorMessage);
    }

}
