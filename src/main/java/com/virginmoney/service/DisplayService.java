package com.virginmoney.service;

import com.virginmoney.model.Transaction;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

public class DisplayService {

    /**
     * Writes the results for a list of transaction objects
     * @param transactions
     */
    public void writeResults(List<Transaction> transactions) {
        System.out.println("");
        for (Transaction transaction : transactions) {
            System.out.println(getTransactionToDisplay(transaction));
        }
    }

    /**
     * Writes the results for a map object
     * @param outputs
     */
    public void writeResults(Map<String, BigDecimal> outputs) {
        System.out.println("");
        outputs.entrySet().forEach(entry->{
            if (!entry.getKey().equals("")) {
                System.out.println(entry.getKey() + ": " + formatAmount(entry.getValue()));
            }
        });
    }

    /**
     * Writes the result for a single transaction
     * @param transaction
     */
    public void writeResults(Transaction transaction) {
        System.out.println("");
        System.out.println(getTransactionToDisplay(transaction));
    }

    /**
     * Utility method to format a date for display purposes
     * @param toConvert
     * @return
     */
    private String formatDate(LocalDate toConvert) {
        return toConvert.format(DateTimeFormatter.ofPattern("dd/MMM/yyyy"));
    }

    /**
     * Utility method to format amount for display purposes
     * @param toConvert
     * @return
     */
    private String formatAmount(BigDecimal toConvert) {
        return NumberFormat.getCurrencyInstance().format(toConvert);
    }

    /**
     * Utility method which writes out the details of the transaction to the console
     * @param transaction
     * @return
     */
    private String getTransactionToDisplay(Transaction transaction) {
        if (transaction == null) {
            return "No transaction found for search terms";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Transaction Date: ");
        sb.append(formatDate(transaction.getTransactionDate()));
        sb.append(" Vendor: ");
        sb.append(transaction.getVendor());
        sb.append(" Type: ");
        sb.append(transaction.getType());
        sb.append(" Amount: ");
        sb.append(formatAmount(transaction.getAmount()));
        sb.append(" Category: ");
        sb.append(transaction.getCategory());
        return sb.toString();
    }
}
