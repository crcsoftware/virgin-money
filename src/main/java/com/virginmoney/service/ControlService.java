package com.virginmoney.service;

import com.virginmoney.model.Transaction;

import java.util.List;

public class ControlService {

    /**
     * Main method for controlling the flow of the application
     */
    public void run() {

        LoadService loadService = new LoadService();
        InputService inputService = new InputService();
        DisplayService displayService = new DisplayService();
        FilterService filterService = new FilterService();

        List<Transaction> transactions = null;
        boolean keepRunning = true;

        try {
            transactions = loadService.getTransactions();
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
            keepRunning = false;
        }

        String category = "";
        int year;
        try {
            while (keepRunning) {
                int action = inputService.getAction();
                switch (action) {
                    case 1:
                        category = inputService.getCategory(transactions);
                        displayService.writeResults(
                                filterService.getAllTransactionsForCategory(category, transactions));
                        break;
                    case 2:
                        displayService.writeResults(
                                filterService.getTotalOutgoingByCategory(transactions));
                        break;
                    case 3:
                        category = inputService.getCategory(transactions);
                        displayService.writeResults(
                                filterService.getMonthlyAverageSpendForCategory(category, transactions));
                        break;
                    case 4:
                        category = inputService.getCategory(transactions);
                        year = inputService.getYear();
                        displayService.writeResults(
                                filterService.getHighestSpendForCategoryAndYear(category, year, transactions));
                        break;
                    case 5:
                        category = inputService.getCategory(transactions);
                        year = inputService.getYear();
                        displayService.writeResults(
                                filterService.getLowestSpendForCategoryAndYear(category, year, transactions));
                        break;
                    case 6:
                        keepRunning = false;
                        inputService.closeScanner();
                        break;
                    default:
                        System.out.println("Invalid option entered");
                        break;
                }
                System.out.println("");
            }
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }
    }
}
