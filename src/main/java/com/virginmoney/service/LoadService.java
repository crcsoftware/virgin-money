package com.virginmoney.service;

import com.virginmoney.exception.ApplicationException;
import com.virginmoney.model.Transaction;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.springframework.util.NumberUtils;
import org.springframework.util.StringUtils;

import java.io.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;

public class LoadService {

    private final static String CSV_FILE_NAME = "transactions.csv";
    private final static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    /**
     * Reads the CSV file and then creates a list of transactions<br>
     * Note that if the transaction has invalid data then it is ignore (must have a
     * date, vendor, type, amount)
     * @return
     * @throws Exception
     */
    public List<Transaction> getTransactions() throws Exception {
        InputStream inputStream = getClass().getClassLoader().
                getResourceAsStream(CSV_FILE_NAME);

        if (inputStream == null) {
            throw new ApplicationException("File not found! " + CSV_FILE_NAME);
        }

        System.out.println("Located the transactions file");

        List<Transaction> transactions = new ArrayList<>();

        Reader in = new InputStreamReader(inputStream);

        Iterable<CSVRecord> records = CSVFormat.DEFAULT.
                withFirstRecordAsHeader().parse(in);

        for (CSVRecord record : records) {
            boolean validTransaction = true;

            String transactionDate = record.get("Transaction Date");
            if (!StringUtils.hasText(transactionDate)) {
                validTransaction = false;
            } else {
                try {
                    LocalDate.parse(transactionDate, formatter);
                } catch (DateTimeParseException exception) {
                    validTransaction = false;
                }
            }

            String vendor = record.get("Vendor");
            if (!StringUtils.hasText(vendor)) {
                validTransaction = false;
            } else {
                vendor = vendor.trim();
            }

            String type = record.get("Type");
            if (!StringUtils.hasText(type)) {
                validTransaction = false;
            } else {
                type = type.trim();
            }

            String amount = record.get("Amount");
            if (!StringUtils.hasText(amount)) {
                validTransaction = false;
            } else {
                try {
                    NumberUtils.parseNumber(amount.trim(), BigDecimal.class);
                } catch (IllegalArgumentException exception) {
                    validTransaction = false;
                }
            }

            String category = record.get("Category");
            if (StringUtils.hasText(category)) {
                category = category.trim();
            }

            if (validTransaction) {

                LocalDate transDate = LocalDate.parse(transactionDate, formatter);
                BigDecimal amountBd = NumberUtils.parseNumber(amount, BigDecimal.class);

                transactions.add(new Transaction(transDate, vendor, type,
                        amountBd, category));
            }
        }

        System.out.println("Completed loading the transactions");
        System.out.println();
        return transactions;
    }


}
