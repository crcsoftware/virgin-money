package com.virginmoney.service;

import com.virginmoney.exception.ApplicationException;
import com.virginmoney.model.Transaction;

import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class InputService {

    Scanner scanner;

    public InputService() {
        scanner = new Scanner(System.in);
    }

    /**
     * Get the action that the user wishes to do
     * @return
     * @throws ApplicationException
     */
    public int getAction() throws ApplicationException {

        System.out.println("Available options");
        System.out.println("1. All transactions for a given category");
        System.out.println("2. Total outgoing per category");
        System.out.println("3. Monthly average spend in a given category");
        System.out.println("4. Highest spend in a given category, for a given year");
        System.out.println("5. Lowest spend in a given category, for a given year");
        System.out.println("6. Terminate application");
        System.out.println("");
        System.out.print("Enter the corresponding number for the information you wish: ");

        try {
            return scanner.nextInt();
        } catch (Exception exception) {
            throw new ApplicationException("Error, invalid option supplied");
        }

    }

    /**
     * Gets the category input by the user, ensures that it's valid
     * @param transactions
     * @return
     */
    public String getCategory(List<Transaction> transactions) {

        String categories = transactions.stream()
                .filter( distinctByKey (Transaction::getCategory) )
                .filter(transaction-> !transaction.getCategory().isEmpty())
                .map(Transaction::getCategory)
                .collect(Collectors.joining(", "));
        System.out.println("Possible categories are " + categories);

        System.out.print("Enter your category to search: ");
        String category = scanner.next();

        return category;
    }

    /**
     * Gets the year input by the user
     * @return
     * @throws ApplicationException
     */
    public int getYear() throws ApplicationException {

        System.out.print("Enter your year to search: ");
        try {
            return scanner.nextInt();
        } catch (Exception exception) {
            throw new ApplicationException("Error, invalid year supplied");
        }
    }

    /**
     * Close the scanner
     */
    public void closeScanner() {
        scanner.close();
    }

    /**
     * Utility method to get only distinct categories for showing options to user
     * @param keyExtractor
     * @param <T>
     * @return
     */
    public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {

        Map<Object, Boolean> seen = new ConcurrentHashMap<>();

        return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }
}
