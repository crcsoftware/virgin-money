package com.virginmoney.service;

import com.virginmoney.model.Transaction;

import java.math.BigDecimal;
import java.time.format.TextStyle;
import java.util.*;
import java.util.stream.Collectors;

public class FilterService {

    /**
     * Gets all transactions which are in a specific category order by latest transaction first
     * @param category
     * @param transactions
     * @return a list of the transactions for the category
     */
    public List<Transaction> getAllTransactionsForCategory(String category, List<Transaction> transactions) {

        return transactions.stream()
                .filter(t -> t.getCategory().equals(category))
                .sorted(Comparator.comparing(Transaction::getTransactionDate).reversed())
                .collect(Collectors.toList());
    }

    /**
     * Gets the total amount for each category
     * @param transactions
     * @return a map contain month as the key and amount as the value
     */
    public Map<String, BigDecimal> getTotalOutgoingByCategory(List<Transaction> transactions) {
        return transactions.stream()
                .collect(Collectors.toMap(Transaction::getCategory,
                                        o -> o.getAmount(),
                                        (o1, o2) -> o1.add(o2)));
    }

    /**
     * Gets the monthly spend for a category then calculates the average based on those months
     * @param transactions
     * @return a map containing month / year as the key and amount as the value
     * plus a final entry which is the average spend based on other keys
     */
    public Map<String, BigDecimal> getMonthlyAverageSpendForCategory(String category, List<Transaction> transactions) {

        Map<String, BigDecimal> monthBreakdown = transactions.stream()
                .filter(t -> t.getCategory().equals(category))
                .collect(Collectors.toMap(
                        transaction -> transaction.getTransactionDate().getMonth()
                                .getDisplayName(TextStyle.SHORT,Locale.ENGLISH) + " "
                                + transaction.getTransactionDate().getYear(),
                        o -> o.getAmount(),
                        (o1, o2) -> o1.add(o2)));

        // get the average across the months
        BigDecimal totalAmount = new BigDecimal(0);
        for (Map.Entry<String, BigDecimal> month : monthBreakdown.entrySet()) {
            totalAmount = totalAmount.add(month.getValue());
        }

        monthBreakdown.put("Monthly average", totalAmount.divide(new BigDecimal(monthBreakdown.size())));

        return monthBreakdown;
    }

    /**
     * Gets the transactions that happened for the category and year supplied then finds the transaction of
     * the highest amount
     * @param category
     * @param year
     * @param transactions
     * @return the transaction with the highest amount for the category and year.
     * null if no transactions found
     */
    public Transaction getHighestSpendForCategoryAndYear(String category, int year, List<Transaction> transactions) {
        return transactions.stream()
                .filter(t -> t.getCategory().equals(category) && t.getTransactionDate().getYear() == year)
                .max(Comparator.comparing(Transaction::getAmount))
                .orElse(null);
    }

    /**
     * Gets the transactions that happened for the category and year supplied then finds the transaction of
     * the smallest amount
     * @param category
     * @param year
     * @param transactions
     * @return the transaction with the smallest amount for the category and year
     * null if no transactions found
     */
    public Transaction getLowestSpendForCategoryAndYear(String category, int year, List<Transaction> transactions) {
        return transactions.stream()
                .filter(t -> t.getCategory().equals(category) && t.getTransactionDate().getYear() == year)
                .min(Comparator.comparing(Transaction::getAmount))
                .orElse(null);
    }

}
