package com.virginmoney;

import com.virginmoney.model.Transaction;
import com.virginmoney.service.FilterService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FilterServiceTest {

    private FilterService filterService;

    @BeforeEach
    public void setUp() throws Exception {
        filterService = new FilterService();
    }

    @Test
    @DisplayName("Get all transactions for a category")
    public void getAllTransactionsForCategoryTest() throws Exception {

        List<Transaction> foundTransactions = filterService.getAllTransactionsForCategory(
                "Groceries", getListOfTransactions());

        // check it's found the correct number of transactions
        assertEquals(2, foundTransactions.size(),
                "Incorrect number of transactions found");

        // check it's ordered by latest transaction first
        assertEquals("Coop", foundTransactions.get(0).getVendor(),
                "Incorrect first transaction");
    }

    @Test
    @DisplayName("Get total outgoings for categories")
    public void getTotalOutgoingByCategoryTest() {

        Map<String, BigDecimal> results = filterService.getTotalOutgoingByCategory(
                 getListOfTransactions());

        // check total for groceries category
        assertEquals(new BigDecimal("96.05"), results.get("Groceries"),
                "Incorrect total for groceries");

        // check total for direct debit category
        assertEquals(new BigDecimal("65"), results.get("DirectDebit"),
                "Incorrect total for direct debits");
    }

    @Test
    @DisplayName("Get monthly average spend for a category")
    public void getMonthlyAverageSpendForCategoryTest() {

        Map<String, BigDecimal> results = filterService.getMonthlyAverageSpendForCategory(
                "Sport", getListOfTransactions());

        // check total for October
        assertEquals(new BigDecimal("30"), results.get("Oct 2020"),
                "Incorrect total for October");

        // check total for November
        assertEquals(new BigDecimal("45"), results.get("Nov 2020"),
                "Incorrect total for November");

        // check total overall average
        assertEquals(new BigDecimal("37.5"), results.get("Monthly average"),
                "Incorrect monthly total average");
    }

    @Test
    @DisplayName("Get highest spend for a category and year")
    public void getHighestSpendForCategoryAndYearTest() throws Exception {

        Transaction foundTransaction = filterService.getHighestSpendForCategoryAndYear(
                "Cinema", 2020, getListOfTransactions());

        // check it's the correct vendor returned
        assertEquals("Showcase", foundTransaction.getVendor(),
                "Incorrect vendor returned");

        // check it's the right amount returned
        assertEquals(new BigDecimal("7"), foundTransaction.getAmount(),
                "Incorrect amount returned");
    }

    @Test
    @DisplayName("Get lowest spend for a category and year")
    public void getLowestSpendForCategoryAndYearTest() throws Exception {

        Transaction foundTransaction = filterService.getLowestSpendForCategoryAndYear(
                "Cinema", 2020, getListOfTransactions());

        // check it's the correct vendor returned
        assertEquals("Vue", foundTransaction.getVendor(),
                "Incorrect vendor returned");

        // check it's the right amount returned
        assertEquals(new BigDecimal("5"), foundTransaction.getAmount(),
                "Incorrect amount returned");
    }

    /**
     * Returns a list of transactions for test purposes
     * @return
     */
    private List<Transaction> getListOfTransactions() {
        List<Transaction> transactions = new ArrayList<>();
        transactions.add(new Transaction(LocalDate.parse("2020-10-20"), "Asda", "card", new BigDecimal(40.5), "Groceries"));
        transactions.add(new Transaction(LocalDate.parse("2020-10-25"), "Coop", "card", new BigDecimal(55.55), "Groceries"));
        transactions.add(new Transaction(LocalDate.parse("2020-10-30"), "Virgin Media", "internet", new BigDecimal(30), "DirectDebit"));
        transactions.add(new Transaction(LocalDate.parse("2021-02-19"), "Sky", "internet", new BigDecimal(35), "DirectDebit"));

        transactions.add(new Transaction(LocalDate.parse("2020-10-30"), "Golf", "internet", new BigDecimal(20), "Sport"));
        transactions.add(new Transaction(LocalDate.parse("2020-10-19"), "Football", "internet", new BigDecimal(10), "Sport"));
        transactions.add(new Transaction(LocalDate.parse("2020-11-30"), "Tennis", "internet", new BigDecimal(40), "Sport"));
        transactions.add(new Transaction(LocalDate.parse("2020-11-19"), "Cycling", "internet", new BigDecimal(5), "Sport"));

        transactions.add(new Transaction(LocalDate.parse("2020-10-30"), "Showcase", "card", new BigDecimal(7), "Cinema"));
        transactions.add(new Transaction(LocalDate.parse("2020-10-19"), "Vue", "card", new BigDecimal(5), "Cinema"));
        transactions.add(new Transaction(LocalDate.parse("2021-11-30"), "Cineworld", "card", new BigDecimal(10), "Cinema"));

        return transactions;
    }



}

