package com.virginmoney;

import com.virginmoney.service.LoadService;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
public class LoadServiceTest {

    private LoadService loadService;

    @BeforeEach
    public void setUp() throws Exception {
        loadService = new LoadService();
    }

    @Test
    @DisplayName("Should create list of transactions")
    public void testLoadProcess() throws Exception {
        assertEquals(13, loadService.getTransactions().size(),
                "Loaded incorrect number of transactions");
    }



}

