# README #

This is my solution to the coding exercise provided by Virgin Money

### Installation ###

* To run the application, open it within your IDE and run the Main class as a java application
* You will then see the application within the console
* You can then enter the relevant options you wish using your keyboard

### Tests ###

* There are simple JUnit tests due to the requirement of "Production Ready"
* These can be run by running the TestSuite application 
